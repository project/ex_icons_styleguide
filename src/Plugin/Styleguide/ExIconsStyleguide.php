<?php

namespace Drupal\ex_icons_styleguide\Plugin\Styleguide;

use Drupal\styleguide\GeneratorInterface;
use Drupal\ex_icons\ExIconsManagerInterface;
use Drupal\styleguide\Plugin\StyleguidePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * External Use Icons as Styleguide.
 *
 * @Plugin(
 *   id = "ex_icons_styleguide",
 *   label = @Translation("External-use Icons")
 * )
 */
class ExIconsStyleguide extends StyleguidePluginBase {

  /**
   * The styleguide generator service.
   *
   * @var \Drupal\styleguide\Generator
   */
  protected $generator;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The external icons manager service.
   *
   * @var \Drupal\ex_icons\ExIconsManagerInterface
   */
  protected $exIconsManager;

  /**
   * Constructs a new StcStyleguide.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\styleguide\GeneratorInterface $styleguide_generator
   *   Styleguide generator.
   * @param \Drupal\ex_icons\ExIconsManagerInterface $ex_icons_manager
   *   Ex icons manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GeneratorInterface $styleguide_generator, ExIconsManagerInterface $ex_icons_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->generator = $styleguide_generator;
    $this->exIconsManager = $ex_icons_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('styleguide.generator'),
      $container->get('ex_icons.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function items() {
    $items = [];

    $items += $this->exIcons();

    return $items;
  }

  /**
   * Icon grid generator.
   *
   * @return array
   *   Items with new elements.
   */
  protected function exIcons() {
    $items = [];

    $available_icons = $this->exIconsManager->getIconOptions();

    if (!empty($available_icons)) {
      $items['ex_icons'] = [
        'title' => $this->t('External-use icons'),
        'content' => [
          '#type' => "container",
          '#attributes' => [
            'style' => 'display:grid;grid-template-columns: repeat(auto-fit, minmax(140px, 1fr));gap: 15px;',
          ],
        ],
        'group' => $this->t('External-use icons'),
      ];

      foreach ($available_icons as $icon) {
        $items['ex_icons']['content'][$icon] = [
          '#type' => "container",
          '#attributes' => [
            'style' => 'text-align:center;aspect-ratio:1;display:flex;flex-direction:column;align-items: center;justify-content: center;border: 1px solid #ccc',
          ],
          'icon' => [
            '#theme' => 'ex_icon',
            '#id' => $icon,
            '#attributes' => [
              'height' => 40,
              'width' => 40,
            ],
          ],
          'title' => [
            '#type' => 'html_tag',
            '#tag' => 'small',
            '#value' => $icon,
            '#attributes' => [
              'style' => 'display:block; margin-top: 8px',
            ],
          ],
        ];
      }
    }

    return $items;
  }

}
